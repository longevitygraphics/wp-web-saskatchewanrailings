<?php if( have_rows('flexible_content_block') ):

	while ( have_rows('flexible_content_block') ) : the_row();
		
		switch ( get_row_layout()) {

			case 'text_block':
				get_template_part('/components/acf-flexible-layout/layouts/text-block');
			break;

			case 'grid_-_2_column':
				get_template_part('/components/acf-flexible-layout/layouts/grid-2-column');
			break;

			case 'grid_-_list':
				get_template_part('/components/acf-flexible-layout/layouts/grid-list');
			break;

			case 'full_width_media':
				get_template_part('/components/acf-flexible-layout/layouts/media');
			break;

			case 'cta_block':
				get_template_part('/components/acf-flexible-layout/layouts/cta-block');
			break;

			case 'gallery_slider':
				get_template_part('/components/acf-flexible-layout/layouts/gallery-slider');
			break;
		}

	endwhile;

	else : // no layouts found 

endif; ?>