<?php 
/**
 * Grid 2 Column Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
<?php 

$images = get_field('gallery_slider','option');
$gallery_title = get_sub_field('gallery_title');

if( $images ): ?>
	<div class='gallery-title col-12 mb-5 m-auto'><?php echo $gallery_title; ?></div>
    <div class="gallery-slider">
        <?php foreach ($images as $key => $image): ?>
        	<?php if($key % 2 == 0): ?>
        		<div>
        	<?php endif; ?>
        	<div><img src="<?php echo $image['url']; ?>"></div>
        	<?php if($key == sizeof($images) - 1): ?>
	        	</div>
	        <?php break; endif; ?>
        	<?php if($key % 2 == 1): ?>
				</div>
        	<?php endif;?>
        <?php endforeach; ?>
    </div> <!-- end of gallery slider -->
<?php endif; ?>

<script>
		(function($) {

		    $(document).ready(function(){

		    	$('.gallery-slider').slick({
					dots: true,
					autoplay: true,
		    		adaptiveHeight: true,
		    		slidesToShow: 4,
		    		slidesToScroll: 1,
		    		infinite: true,
		    		arrows:true,
		    		responsive: [
				    {
				      breakpoint: 1024,
				      settings: {
				        slidesToShow: 3,
				        slidesToScroll: 1,
				        infinite: true,
				        dots: true
				      }
				    },
				    {
				      breakpoint: 768,
				      settings: {
				        slidesToShow: 2,
				        slidesToScroll: 1
				      }
				    },
				    {
				      breakpoint: 500,
				      settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1,
				        arrows:false
				      }
				    }]
				});
		  });
		}(jQuery));
</script>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>
