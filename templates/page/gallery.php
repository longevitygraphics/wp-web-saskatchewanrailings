<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<div class="container py-4">

					<h2 class="h2 text-center mb-4">Glass Railing Gallery</h2>

					<?php 

					$images = get_field('image_gallery');
					$size = 'full'; // (thumbnail, medium, large, full or custom size)

					if( $images ): ?>
					    <div class="gallery-grid">
					        <?php foreach( $images as $image ): ?>
					            <div class="grid-item">
					            	<a data-lightbox="glass-railing" href="<?php echo $image['url']; ?>"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></a>
					            </div>
					        <?php endforeach; ?>
					    </div>
					<?php endif; ?>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>