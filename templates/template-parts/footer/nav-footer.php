<h2 class="nav-title">Quick Links</h2>
<?php
	wp_nav_menu( array(
		'theme_location' => 'menu-1',
		'menu_id'        => 'primary-menu',
		'depth'          => 1,
		'container'      => 'nav'
	) );
?>