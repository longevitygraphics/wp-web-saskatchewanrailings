<div class="py-4 bg-primary">
	<div class="container cta-block d-flex flex-wrap justify-content-start justify-content-md-between text-white align-items-center">
		<?php if($cta_title): ?>
			<div class="h3 my-2 text-white"><?php echo $cta_title; ?></div>
		<?php endif; ?>

		<?php if($cta_button): ?>
			<a href="<?php echo $cta_button['url']; ?>" class="btn btn-secondary mx-1 h4 mb-0"><?php echo $cta_button['title']; ?></a>
		<?php endif; ?>
	</div>
</div>