<div class="our-locations">
	<div class="container">

		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'		=> 'location',
		    );

		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :
		    	?>
		    	
				<div class="our-location-loop row">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post(); 
		    	$title = get_the_title();
		    	$link = get_permalink();
		    	$icon = get_field('icon')['url'];
		    	?>
		    		<div class="col-12 col-sm-6 col-lg-4">
		    			<a href="<?php echo get_permalink(); ?>">
			    				<div class="single-location" style="background-image: url(<?php echo $icon; ?>);">
			    				<?php echo $title; ?>
			    			</div>
			    		</a>
		    		</div>
		    	<?php
		        endwhile;
		        ?>
		        </div>
		    <?php

		    endif; // End Loop

		    wp_reset_query();
		?>

	</div>
</div>