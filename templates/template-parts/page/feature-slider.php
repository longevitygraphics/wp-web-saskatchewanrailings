<?php
	$feature_slider_active = get_field('feature_slider_active');
	$form_active = get_field('form_active');
	$top_banner_text = get_field('top_banner_text');
?>

<?php

if( have_rows('feature_slider') && $feature_slider_active == 1 ):
	?>
		<div class="feature-slider-wrap">
			<div class="feature-slider">
				<?php
				    while ( have_rows('feature_slider') ) : the_row();
				        $image = get_sub_field('image');
				        ?>
				        <div><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
				        <?php
				    endwhile;
			    ?>
		    </div>

		    <?php if($form_active): ?>
			    <div class="overlay_wrap py-4">
				    <div class="overlay container align-items-center justify-content-center justify-content-md-end">
				    	<?php if ($top_banner_text): ?>
					    	<div class="top-banner-text"><?php echo $top_banner_text; ?></div>
					    <?php endif ?>
					    <?php if($form_active): ?>
					    	<div class="d-none d-md-block"><?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?></div>
					    <?php endif; ?>
				    </div>
			    </div>
			<?php endif; ?>

	    </div>
    <?php
else :
    // no rows found
endif;

?>