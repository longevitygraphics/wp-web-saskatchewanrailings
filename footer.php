<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<?php

		$cta_title = get_field('footer_cta', 'option')['title'];
		$cta_button = get_field('footer_cta', 'option')['button'];

		include(locate_template('/templates/template-parts/footer/cta-block.php')); 

	?>

	<footer id="colophon" class="site-footer text-center text-md-left">
		
		<div id="site-footer" class="py-4">
			<div class="container">
				<div class="site-footer-alpha"><?php dynamic_sidebar('footer-alpha'); ?></div>
				<div class="site-footer-bravo"><?php dynamic_sidebar('footer-bravo'); ?></div>
				<div class="site-footer-charlie"><?php dynamic_sidebar('footer-charlie'); ?></div>
			</div>
		</div>

		<div id="site-legal">
			<div class="container py-2">
				<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
				<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
			</div>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>
<div class="lg-scroll-to-top">
	<i class="fas fa-chevron-up"></i>
</div>
</body>
</html>
