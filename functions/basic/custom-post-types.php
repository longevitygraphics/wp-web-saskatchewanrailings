<?php

    // SERVICE
    
    function lg_custom_post_type(){
      register_post_type( 'location',
          array(
            'labels' => array(
              'name' => __( 'Locations' ),
              'singular_name' => __( 'Location' )
            ),
            'public' => true,
            'has_archive' => true,
            'menu_icon'   => 'dashicons-location',
            "rewrite" => array("with_front" => false, "slug" => 'locations'),
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );
      register_post_type( 'service',
          array(
            'labels' => array(
              'name' => __( 'Services' ),
              'singular_name' => __( 'Service' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'   => 'dashicons-location',
            'show_in_menu'    => 'lg_menu',
            'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
          )
      );

    }

    add_action( 'init', 'lg_custom_post_type' );

?>