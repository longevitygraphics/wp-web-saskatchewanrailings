// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

        $('.dropdown-toggle').on('mouseover', function(e){
          $(this).siblings('.dropdown-menu').addClass('show');
        });

        $('.dropdown-toggle').on('mouseleave', function(e){
          $(this).siblings('.dropdown-menu').removeClass('show');
        });

        //scroll to top
        $(window).scroll(function(){
            if ($(this).scrollTop() < 200) {
                $('.lg-scroll-to-top') .fadeOut();
            } else {
                $('.lg-scroll-to-top') .fadeIn();
            }
        });
        $('.lg-scroll-to-top').on('click', function(){
            $('html, body').animate({scrollTop:0}, 'fast');
            return false;
        });

        $('.feature-slider').slick({
          autoplay: true,
          fade: true,
          arrows: true,
          dots: false,
          autoplaySpeed: 6000,
          adaptiveHeight: true
        });

        lightbox.option({
          'resizeDuration': 700,
          'wrapAround': true,
          'disableScrolling': true,
          'positionFromTop': 0
        })

        //Mega Menu
        if($('.mega-menu-toggle')[0]){
          $('.mobile-toggle').on('click', function(){
            $('.mega-menu-toggle').click();
          });
        }

        $('.product_button').mouseover(function(){
          $(this).addClass('hover');
        });

        $('.product_button').mouseleave(function(){
          $(this).removeClass('hover');
        });

        $('.product-options select').selectric();
        $('select.orderby').selectric();

        $('.our-testimonials-loop').masonry({
          // options
          itemSelector: '.our-testimonials-loop >div'
        });

        //Woocommerce

        if($('.single-product-top')[0]){
          var quantity = $('.input-text.qty');
          $('.quantity_field .value').append(quantity);

          $('.single-color img').on('click', function(){
            var value = $(this).closest('.single-color').attr('value');
            if($('.variations input[name=_color]').val() != value){
              $('.variations input[name=_color]').val(value);
              $(this).closest('.single-color').addClass('active').siblings().removeClass('active');
            }
          });

          $('form.cart').submit(function(){
            var color = $('.product-options').find('input[name=_color]').val();
            var shape = $('.product-options').find('select[name=_shape]').val();
            var length = $('.product-options').find('select[name=_length]').val();
            var cap = $('.product-options').find('select[name=_cap]').val();
            var use = $('.product-options').find('select[name=_use]').val();
            var surface = $('.product-options').find('select[name=_surface]').val();

            var check_list = [
              {'name': 'Color', 'value': color},
              {'name': 'Shape', 'value': shape},
              {'name': 'Length', 'value': length},
              {'name': 'Cap', 'value': cap},
              {'name': 'Use', 'value': use},
              {'name': 'Surface', 'value': surface}
            ];

            var error_message = '<hr><div class="single_product_error_message text-danger">';
            var success = true;

            check_list.map(function(item){
              if(!item.value || item.value == '' || item.value == -1){
                error_message += '<div><strong>' + item.name + '</strong> is required.</div>';
                success = false;
              }
            });

            error_message += '</div>';

            if(success)
              return true;
            else
              $('.single-product-error-message').html(error_message);
              return false;
          });
        }
        
    });

}(jQuery));