function sticky_header_initialize(){

	if(jQuery('.sticky-header')[0]){
		var headerHeight = jQuery('#masthead').outerHeight();
		jQuery('.site-content').css('paddingTop', headerHeight);
		jQuery('#masthead').fadeIn();
	}
}
